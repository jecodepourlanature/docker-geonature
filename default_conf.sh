#!/bin/bash

if [ ! -f config/geonature_config.toml ]; then
	echo "Création du fichier de configuration ..."
	cp config/geonature_config.toml.sample config/geonature_config.toml
	echo "Préparation du fichier de configuration..."
	sed -i "s|SQLALCHEMY_DATABASE_URI = .*$|SQLALCHEMY_DATABASE_URI = \"${DATABASE_URI}\"|g" config/geonature_config.toml
	sed -i "s|URL_APPLICATION = .*$|URL_APPLICATION = '${URL_APPLICATION}'|g" config/geonature_config.toml
	sed -i "s|API_ENDPOINT = .*$|API_ENDPOINT = '${URL_APPLICATION}/api'|g" config/geonature_config.toml
	sed -i "s|API_TAXHUB = .*$|API_TAXHUB = '${API_TAXHUB}'|g" config/geonature_config.toml
	sed -i "s|DEFAULT_LANGUAGE = .*$|DEFAULT_LANGUAGE = '${DEFAULT_LANGUAGE}'|g" config/geonature_config.toml
	sed -i "s|LOCAL_SRID = .*$|LOCAL_SRID = '${LOCAL_SRID}'|g" config/geonature_config.toml
	sed -i "s|SECRET_KEY = .*|SECRET_KEY = '`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1`'|g" config/geonature_config.toml
else
	echo "Le fichier de configuration existe déjà"
fi

