#!/bin/bash

FLASKDIR=/opt/gn/geonature/backend
APP_DIR=/opt/gn/geonature

app_name="geonature"
gun_num_workers=$GUN_NUM_WORKERS
gun_host=0.0.0.0
gun_port=5000

echo "Starting $app_name"
echo "$FLASKDIR"
echo "$(dirname $0)/config/settings.ini"
echo $APP_DIR
. $APP_DIR/config/settings.ini

# activate the virtualenv
source $FLASKDIR/$venv_dir/bin/activate

cd $FLASKDIR

# Start your gunicorn
exec gunicorn  wsgi:app --pid="${app_name}.pid" -w "${gun_num_workers}"  -b "${gun_host}:${gun_port}"  -n "${app_name}"

