FROM debian:stretch

RUN apt-get update && apt-get install -y  python3 python3-dev python3-setuptools python-pip \
	libpq-dev libgdal-dev python-gdal python-virtualenv build-essential \
	postgresql-client postgis supervisor apache2 git curl apt-transport-https
RUN pip install --upgrade pip virtualenv virtualenvwrapper
RUN curl -sSL https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
ENV NODE_VERSION=node_8.x
RUN DISTRO="$(lsb_release -s -c)" ;\
	echo "deb https://deb.nodesource.com/$NODE_VERSION $DISTRO main" | tee /etc/apt/sources.list.d/nodesource.list ; \
	echo "deb-src https://deb.nodesource.com/$NODE_VERSION $DISTRO main" | tee -a /etc/apt/sources.list.d/nodesource.list ; \
	apt-get update && apt-get install -y npm
ENV GN_VERSION=2.0.0-rc.4.2
RUN mkdir -p /opt/gn/geonature 
WORKDIR /opt/gn/geonature
RUN curl -sSL https://github.com/PnX-SI/GeoNature/archive/$GN_VERSION.tar.gz > app.tar.gz
RUN tar --strip-components=1 -zxf app.tar.gz

# installation
RUN cd backend; virtualenv -p python3 venv
RUN apt-get install -y locales -qq && locale-gen en_US.UTF-8 en_us && dpkg-reconfigure locales && dpkg-reconfigure locales && locale-gen C.UTF-8 && /usr/sbin/update-locale LANG=C.UTF-8
ENV LANG C.UTF-8
ENV LANGUAGE C.UTF-8
ENV LC_ALL C.UTF-8
RUN cd backend; bash -c "source ./venv/bin/activate;  which pip; pip --version;  pip install -r requirements.txt"
RUN cd backend; bash -c "source ./venv/bin/activate; cd .. ; python geonature_cmd.py install_command"

ENV DATABASE_URI="postgresql://user:password@postgres:port/dbname"
ENV URL_APPLICATION="http://example.com/geonature"
ENV API_TAXHUB="http://example.com/taxhub"
ENV DEFAULT_LANGUAGE="fr"
ENV LOCAL_SRID="2154"
ADD default_conf.sh ./default_conf.sh
RUN bash default_conf.sh
RUN bash -c "source backend/venv/bin/activate; geonature generate_frontend_config --conf-file config/geonature_config.toml --build=false"
EXPOSE 5000
ADD gunicorn_start.sh /usr/local/bin/gunicorn_start.sh
RUN chmod a+x /usr/local/bin/gunicorn_start.sh
ADD supervisor-geonature.conf /etc/supervisor/conf.d/geonature-service.conf
CMD supervisord -c /etc/supervisor.conf

